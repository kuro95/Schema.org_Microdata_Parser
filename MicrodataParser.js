// Start exports
exports.parseSite = function (site) {
    return parseSite(site);
}

exports.parseFile = function (file) {
    return parseFile(file);
}

exports.parse = function (arg, name) {
    return parse(arg, name);
}

exports.getData = function () {
    while (!data) {
        sleepData();
    }
    return data;
}

// Start program
var data;
html = "";

var microdata = require('microdata-node');

const util = require('util');

var http = require('follow-redirects').http;

// Sleeps for a periodic time to check for output data
function sleepData() {
    var ret;
    setTimeout(function () {
        ret = "hello";
    }, 250);
    while (ret === undefined) {
        require('deasync').runLoopOnce();
    }
    return ret;
}

// Converts the microdata objects to JSON-LD format
function toJSONLD(html) {
    var output = microdata.toJsonld(html, {
        base: 'http://www.example.com'
    });

    for (var i = 0; i < output.length; ++i) {
        var obj = output[i];

        for (var attributename in obj) {
            if (attributename.charAt(0) != '@') {
                var new_name = attributename.slice(18);
                obj[new_name] = obj[attributename];
                delete obj[attributename];
                attributename = new_name;
            }
            if (Array.isArray(obj[attributename])) {
                for (var j = 0; j < obj[attributename].length; ++j) {
                    if (obj[attributename][j]['@id']) {
                        var id = obj[attributename][j]['@id']
                        for (var k = 0; k < output.length; ++k) {
                            if (output[k] && output[k]['@id'] === id) {
                                obj[attributename] = output[k];
                                delete output[k];
                            }
                        }
                    }
                }
            }
        }
        if (obj) {
            obj['@type'] = obj['@type'][0].slice(18);
            obj['@context'] = "http://schema.org/";
        }
    }
    data = JSON.stringify(output[0], null, 2);
}

//Splits a value on a given index
function splitValue(value, index) {
    return [value.substring(0, index), value.substring(index)];
}

function parse(argument, name) {

    // Check if enough parameters were given
    if (argument === "-u") {
        parseSite(name);
    } else if (argument === "-f") {
        parseFile(name);
    } else {
        console.log("Wrong parameter: -u for Website or -f for File, or use parseSite/parseFile");
        process.exit(0);
    }
};

function parseSite(name) {
    var parseport = 80;
    // Filter out the http:// or https:// part if there is any.
    var httpparts = ["", ""];
    if (name.indexOf('://') > -1) {
        httpparts = splitValue(name, name.indexOf('://'));
        httpparts[1] = httpparts[1].substring(3);
    } else {
        httpparts[1] = name;
    }
    // Changes for https connection
    if (httpparts[0] === 'https') {
        parseport = 443;
        http = require('follow-redirects').https;
    }
    // Split the host and path in the remaining name.
    var urlparts = ["", ""];
    if (httpparts[1].indexOf('/') > -1) {
        urlparts = splitValue(httpparts[1], httpparts[1].indexOf('/'));
    } else {
        urlparts[0] = httpparts[1];
        urlparts[1] = "/";
    }
    // Define options for our http(s) request
    var options = {
        host: urlparts[0],
        port: parseport,
        path: urlparts[1]
    };
    requestSite(options);
}

function parseFile(name) {
    const fs = require('fs');
    // Check if file exists
    fs.access(name, fs.constants.R_OK | fs.constants.W_OK, (err) => {
        if (err != null) {
            data = err;
        } else {
            requestFile(name);
        }
    });
}

//Get full html data
function requestSite(options) {
    http.get(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            html += chunk.toString('utf8');
        }).on("end", function () {
            toJSONLD(html);
        });
    }).on('error', function (e) {
        console.log("Got error: " + e.message);
    });
}

//Get html data out of file
function requestFile(name) {
    fs = require('fs')
    fs.readFile(name, 'utf8', function (err, data) {
        if (err) {
            data = err;
            return console.log(err);
        }
        toJSONLD(data);
    });
}