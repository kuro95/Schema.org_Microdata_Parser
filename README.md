# Schema.org Microdata Parser

This parser can be used in order to parse html content from a website or a file. 

The project was programmed by 3 computer science students of the University of Innsbruck.


# Usage


#### parseSite("www.schema.org"), parse("-u", "www.schema.org")
    Parses a website for schema.org annotations in microdata format.
    Sites can also start with http(s).
        
        
#### parseFile("C:\\\\tmp\\\\index.html"), parse("-f", "C:\\\\tmp\\\\index.html")
    Parses a html file for schema.org annotations in microdata format.
    The path can either be absolute or relative to the running program.
        
#### getData()
    Returns the Schmema.org data of the website in JSON-LD format.
    Contains an error message if something did not work.

# Example

    var parser = require('microdata_parser');
    
    parser.parserSite("www.google.de");
    parser.getData();
